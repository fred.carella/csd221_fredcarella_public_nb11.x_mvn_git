/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture4.ex2021;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author students
 */
public class App {

    private final List<SaleableItem> items;
    private final CashTill ct;
    
    public App() {
        this.items = new ArrayList<>();
        this.ct=new CashTill();
    }
    public void run(){
        
        Ticket t=new Ticket();
        items.add(t);
        Book b=new Book();
        items.add(b);
        
        ct.sellItem(t);
        ct.sellItem(b);
        
        for(SaleableItem item:items){
            System.out.println("Price in items list ="+item.getPrice());
        }
    }
    
}
