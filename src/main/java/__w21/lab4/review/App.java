/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lab4.review;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author students
 */
public class App {
    List<SaleableItem> items=new ArrayList<>();
    CashTill till=new CashTill();
    public void run() {
        
        Candy c=new Candy();
        Ticket i1 = new Ticket();
        Book i2 = new Book();
        i1.setPrice(20.20);
        i2.setPrice(10.10);
        items.add(i1);
        items.add(i2);
        items.add(c);
        
        for(SaleableItem item:items){
            till.sellItem(item);
        }
        
    }
    
}
