/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lab4.review;

/**
 *
 * @author students
 */
public class Candy implements SaleableItem{

    @Override
    public void sellCopy() {
        System.out.println("selling candy");
    }

    @Override
    public double getPrice() {
        return .50;
    }
    
}
