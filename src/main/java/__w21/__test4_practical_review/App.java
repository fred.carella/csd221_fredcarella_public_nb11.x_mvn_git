/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__test4_practical_review;

import __w21.__test4_practical_review.entities.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author students
 */
public class App {

    private EntityManagerFactory emf;
    //w21_test4_practical_PU
    private EntityManager em;

    void run() {

        try {
            emf = Persistence.createEntityManagerFactory("w21_test4_practical_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created (" + emf + ")");
            em.getTransaction().begin();
            Book_p book = new Book_p();
            book.setAuthor("Me");
            book.setTitle("A Book");
            book.setCopies(10);
            book.setPrice(20.00);

            Magazine_p mag = new Magazine_p();
            mag.setTitle("A mag");

            Magazine_p mag2 = new Magazine_p();
            mag.setTitle("A mag2");
            em.persist(book);
            em.persist(mag);
            em.persist(mag2);
//            System.out.println("Book=" + book);
            em.getTransaction().commit();
            
            em.getTransaction().begin();
            book.setAuthor("Joshua Bloch");
            em.merge(book);
            em.getTransaction().commit();
            List<Publication_p> ListOfPublication_ps = em.createQuery("SELECT c FROM Publication_p c").getResultList();
            System.out.println("List of Publication_ps");
            for(Publication_p customer:ListOfPublication_ps){
                System.out.println(customer.getTitle());
            }
 
//            Customer c=new Customer();
//            c.setName("Fred");
//            c.setAge(54);
//            c.setAddress(new Address());
//            c.getAddress().setCity("SSM");
//            c.getAddress().setStreet("Ashmun2");
//            c.getAddress().setState("MI");
//            em.persist(c);
//            Customer c2=new Customer();
//            c2.setName("Carella");
//            c2.setAge(55);
//            em.persist(c2);
//            Employee e=new Employee();
//            e.setName("FredEmp");
//            e.setAge(54);
//            
//            Department d=new Department();
//            d.setName("fred");
//            em.persist(d);
//            
//            ProductOrder po=new ProductOrder();
//            po.setName("poname");
//            em.persist(po);
//
//            em.persist(e);
//            em.getTransaction().commit();
//            
//            
//            List<Customer> ListOfCustomers = em.createQuery("SELECT c FROM Customer c").getResultList();
//            System.out.println("List of Customers");
//            for(Customer customer:ListOfCustomers){
//                System.out.println(customer.getName());
//            }
        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }

    }

}
