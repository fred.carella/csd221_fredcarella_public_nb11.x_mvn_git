/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__test4_practical_review;

import __w21.__test4_practical_review.controllers.Book_pJpaController;
import __w21.__test4_practical_review.controllers.Magazine_pJpaController;
import __w21.__test4_practical_review.controllers.Publication_pJpaController;
import __w21.__test4_practical_review.entities.Book_p;
import __w21.__test4_practical_review.entities.Magazine_p;
import __w21.__test4_practical_review.entities.Publication_p;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author students
 */
public class AppWithControllers {
    private EntityManagerFactory emf;
    //w21_test4_practical_PU
    private EntityManager em;

    void run() {

        try {
            emf = Persistence.createEntityManagerFactory("w21_test4_practical_PU");
            em = emf.createEntityManager();
            Logger.getLogger(MainWithControllers.class.getName()).log(Level.INFO, "Entity Manager created (" + emf + ")");
            Book_p book = new Book_p();
            book.setAuthor("Me");
            book.setTitle("A Book");
            book.setCopies(10);
            book.setPrice(20.00);

            Magazine_p mag = new Magazine_p();
            mag.setTitle("A mag");

            Magazine_p mag2 = new Magazine_p();
            mag.setTitle("A mag2");
            
            Book_pJpaController bookController=new Book_pJpaController(emf);
            Magazine_pJpaController magController=new Magazine_pJpaController(emf);
            
            bookController.create(book);
            magController.create(mag);
            magController.create(mag2);
            
            bookController.destroy(book.getId());
            
            book.setAuthor("Some Author");
            bookController.edit(book);
            
            Publication_pJpaController p=new Publication_pJpaController(emf);
            List<Publication_p> pubs=p.findPublication_pEntities();
            for(Publication_p p1:pubs){
                System.out.println("Title="+p1.getTitle());
            }
            
            List<Book_p> pubs2=bookController.findBook_pEntities();
            for(Book_p p1:pubs2){
                System.out.println("Title="+p1.getTitle());
            }
            
            
        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }
}
