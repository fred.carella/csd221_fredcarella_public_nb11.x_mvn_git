/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__test4_practical_review.entities;

/**
 *
 * @author students
 */
public interface SaleableItem {
    public void sellItem();
    public double getPrice();
}
