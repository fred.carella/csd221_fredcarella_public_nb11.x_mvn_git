package __w21.__test4_practical_review.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author students
 */

@Entity
public class DiscMag_p extends Magazine_p implements Serializable {

    @Basic
    private boolean hasDisc;

    public boolean isHasDisc() {
        return hasDisc;
    }

    public void setHasDisc(boolean hasDisc) {
        this.hasDisc = hasDisc;
    }

}