/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__test4_practical_review.controllers;

import __w21.__test4_practical_review.controllers.exceptions.NonexistentEntityException;
import __w21.__test4_practical_review.entities.Book_p;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Book_pJpaController implements Serializable {

    public Book_pJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Book_p book_p) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(book_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Book_p book_p) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            book_p = em.merge(book_p);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = book_p.getId();
                if (findBook_p(id) == null) {
                    throw new NonexistentEntityException("The book_p with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Book_p book_p;
            try {
                book_p = em.getReference(Book_p.class, id);
                book_p.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The book_p with id " + id + " no longer exists.", enfe);
            }
            em.remove(book_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Book_p> findBook_pEntities() {
        return findBook_pEntities(true, -1, -1);
    }

    public List<Book_p> findBook_pEntities(int maxResults, int firstResult) {
        return findBook_pEntities(false, maxResults, firstResult);
    }

    private List<Book_p> findBook_pEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Book_p as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Book_p findBook_p(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Book_p.class, id);
        } finally {
            em.close();
        }
    }

    public int getBook_pCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Book_p as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
