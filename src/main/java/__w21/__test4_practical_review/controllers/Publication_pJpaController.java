/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__test4_practical_review.controllers;

import __w21.__test4_practical_review.controllers.exceptions.NonexistentEntityException;
import __w21.__test4_practical_review.entities.Publication_p;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Publication_pJpaController implements Serializable {

    public Publication_pJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Publication_p publication_p) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(publication_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Publication_p publication_p) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            publication_p = em.merge(publication_p);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = publication_p.getId();
                if (findPublication_p(id) == null) {
                    throw new NonexistentEntityException("The publication_p with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Publication_p publication_p;
            try {
                publication_p = em.getReference(Publication_p.class, id);
                publication_p.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The publication_p with id " + id + " no longer exists.", enfe);
            }
            em.remove(publication_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Publication_p> findPublication_pEntities() {
        return findPublication_pEntities(true, -1, -1);
    }

    public List<Publication_p> findPublication_pEntities(int maxResults, int firstResult) {
        return findPublication_pEntities(false, maxResults, firstResult);
    }

    private List<Publication_p> findPublication_pEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Publication_p as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Publication_p findPublication_p(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Publication_p.class, id);
        } finally {
            em.close();
        }
    }

    public int getPublication_pCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Publication_p as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
