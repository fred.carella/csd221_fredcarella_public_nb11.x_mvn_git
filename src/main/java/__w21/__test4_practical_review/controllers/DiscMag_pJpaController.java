/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__test4_practical_review.controllers;

import __w21.__test4_practical_review.controllers.exceptions.NonexistentEntityException;
import __w21.__test4_practical_review.entities.DiscMag_p;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class DiscMag_pJpaController implements Serializable {

    public DiscMag_pJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DiscMag_p discMag_p) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(discMag_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DiscMag_p discMag_p) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            discMag_p = em.merge(discMag_p);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = discMag_p.getId();
                if (findDiscMag_p(id) == null) {
                    throw new NonexistentEntityException("The discMag_p with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DiscMag_p discMag_p;
            try {
                discMag_p = em.getReference(DiscMag_p.class, id);
                discMag_p.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The discMag_p with id " + id + " no longer exists.", enfe);
            }
            em.remove(discMag_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DiscMag_p> findDiscMag_pEntities() {
        return findDiscMag_pEntities(true, -1, -1);
    }

    public List<DiscMag_p> findDiscMag_pEntities(int maxResults, int firstResult) {
        return findDiscMag_pEntities(false, maxResults, firstResult);
    }

    private List<DiscMag_p> findDiscMag_pEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from DiscMag_p as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DiscMag_p findDiscMag_p(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DiscMag_p.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiscMag_pCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from DiscMag_p as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
