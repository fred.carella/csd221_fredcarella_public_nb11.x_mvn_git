/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__test4_practical_review.controllers;

import __w21.__test4_practical_review.controllers.exceptions.NonexistentEntityException;
import __w21.__test4_practical_review.entities.Ticket_p;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Ticket_pJpaController implements Serializable {

    public Ticket_pJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ticket_p ticket_p) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ticket_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ticket_p ticket_p) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ticket_p = em.merge(ticket_p);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = ticket_p.getId();
                if (findTicket_p(id) == null) {
                    throw new NonexistentEntityException("The ticket_p with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ticket_p ticket_p;
            try {
                ticket_p = em.getReference(Ticket_p.class, id);
                ticket_p.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ticket_p with id " + id + " no longer exists.", enfe);
            }
            em.remove(ticket_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ticket_p> findTicket_pEntities() {
        return findTicket_pEntities(true, -1, -1);
    }

    public List<Ticket_p> findTicket_pEntities(int maxResults, int firstResult) {
        return findTicket_pEntities(false, maxResults, firstResult);
    }

    private List<Ticket_p> findTicket_pEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Ticket_p as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ticket_p findTicket_p(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ticket_p.class, id);
        } finally {
            em.close();
        }
    }

    public int getTicket_pCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Ticket_p as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
