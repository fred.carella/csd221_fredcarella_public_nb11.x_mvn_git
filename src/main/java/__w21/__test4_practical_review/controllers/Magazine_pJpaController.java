/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__test4_practical_review.controllers;

import __w21.__test4_practical_review.controllers.exceptions.NonexistentEntityException;
import __w21.__test4_practical_review.entities.Magazine_p;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Magazine_pJpaController implements Serializable {

    public Magazine_pJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Magazine_p magazine_p) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(magazine_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Magazine_p magazine_p) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            magazine_p = em.merge(magazine_p);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = magazine_p.getId();
                if (findMagazine_p(id) == null) {
                    throw new NonexistentEntityException("The magazine_p with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Magazine_p magazine_p;
            try {
                magazine_p = em.getReference(Magazine_p.class, id);
                magazine_p.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The magazine_p with id " + id + " no longer exists.", enfe);
            }
            em.remove(magazine_p);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Magazine_p> findMagazine_pEntities() {
        return findMagazine_pEntities(true, -1, -1);
    }

    public List<Magazine_p> findMagazine_pEntities(int maxResults, int firstResult) {
        return findMagazine_pEntities(false, maxResults, firstResult);
    }

    private List<Magazine_p> findMagazine_pEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Magazine_p as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Magazine_p findMagazine_p(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Magazine_p.class, id);
        } finally {
            em.close();
        }
    }

    public int getMagazine_pCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Magazine_p as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
