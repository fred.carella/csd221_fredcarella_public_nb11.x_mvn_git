package __w21.lecture2.lecture2.jpa_example.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @author students
 */

@Entity
public class Car20 extends Vehicle20 implements Serializable {

    @Basic
    private String make;
    @Basic
    @Column(name = "model")
    private String model;
    @Basic
    private int year;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}