/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture2.lecture2.jpa_example;

import __w21.lecture2.lecture2.jpa_example.entities.Car20;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author students
 */
public class Main {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("w20_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created (" + emf + ")");
            em.getTransaction().begin();
            Car20 mustang=new Car20();
            mustang.setMake("Ford");
            mustang.setModel("Mustang");
            mustang.setYear(2021);
            mustang.setSerial_number("12345");
            em.persist(mustang);
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
