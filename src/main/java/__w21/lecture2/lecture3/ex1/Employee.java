/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture2.lecture3.ex1;

/**
 *
 * @author students
 */
public class Employee extends Person {
    private String employer;

    public Employee(String firstname, String lastname) {
        super(firstname, lastname);
        System.out.println("Employer default constructor");
    }

    public Employee(String employer, String firstname, String lastname) {
        super(firstname, lastname);
        System.out.println("Employer(String) constructor");
        
        this.employer = employer;
    }

    @Override
    public String toString() {
//        return super.toString(); //To change body of generated methods, choose Tools | Templates.
        return getEmployer()+": "+super.toString();
    }
    
    /**
     * @return the employer
     */
    public String getEmployer() {
        return employer;
    }

    /**
     * @param employer the employer to set
     */
    public void setEmployer(String employer) {
        this.employer = employer;
    }
    
    
}
