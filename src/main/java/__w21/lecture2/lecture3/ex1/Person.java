/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture2.lecture3.ex1;

/**
 *
 * @author students
 */
public class Person {
    private String firstname;
    private String lastname;

    public Person() {
        System.out.println("in Person constructor");
        firstname="no firstname";
        lastname="no lastname";
    }

    public  Person(String firstname, String lastname) {
        System.out.println("in Person(String, String) constructor");
        this.firstname = firstname;
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return "Person="+getFirstname()+" "+getLastname();
    }
    

    

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    
    
    
}
