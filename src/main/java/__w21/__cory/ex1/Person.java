/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__cory.ex1;

/**
 *
 * @author students
 */
public class Person extends Object {
    private String firstname;// instance variable.  every object has its own copy
    private String lastname;
    private String SocialInsuranceNumber;
    private int age;
    public static int COUNT;
    
    public Person(){
        COUNT++;
    }
    
    @Override
    public String toString(){
        return getFirstname()+" "+getLastname();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        
        if(! (obj instanceof Person))
            return false;
        Person p=(Person)obj;
        if(p.SocialInsuranceNumber.equals(this.getSocialInsuranceNumber()))
            return true;
        
        return false;
    }
    
    
    
    public Person(String firstname, String lastname){
        this.firstname=firstname;
        this.lastname=lastname;
        COUNT++;
    }
    public Person(String firstname, String lastname, String SocialInsuranceNumber, int age){
        this.firstname=firstname;
        this.lastname=lastname;
        this.SocialInsuranceNumber=SocialInsuranceNumber;
        this.age=age;
        COUNT++;
    }
    
    public void setFirstname(String firstname){// local variable firstname
        this.firstname=firstname;
    }
    public String getFirstname(){
        return firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the SocialInsuranceNumber
     */
    public String getSocialInsuranceNumber() {
        return SocialInsuranceNumber;
    }

    /**
     * @param SocialInsuranceNumber the SocialInsuranceNumber to set
     */
    public void setSocialInsuranceNumber(String SocialInsuranceNumber) {
        this.SocialInsuranceNumber = SocialInsuranceNumber;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age){
        if(age<=0)
            System.out.println("Negative age not allowed");
        this.age = age;
    }

}
