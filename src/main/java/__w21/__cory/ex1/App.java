/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__cory.ex1;

/**
 *
 * @author students
 */
public class App {

    public void run() {
        System.out.println("Running App...");
        System.out.println(Person.COUNT);
        Person cory = new Person();
        cory.setFirstname("Cory");
        cory.setLastname("Peltier");
        cory.setSocialInsuranceNumber("1234");
        cory.setAge(25);
        
        Person cory4=new Person("Cory", "Peltier", "1234", 25);
        
        System.out.println(Person.COUNT);
        Person fred = new Person("Fred", "Carella");
        fred.setSocialInsuranceNumber("0989234812384-");
        System.out.println(Person.COUNT);
        
        Person alison=new Person("Alison", "Nadjiwon", "8231840-128490",27);
        System.out.println(Person.COUNT);
        
        System.out.println(Person.COUNT);
        
        System.out.println("Fred "+fred);
        
        
        Person cory2=new Person("Cory","Peltier");
        cory2.setSocialInsuranceNumber("4567");
        
        Person cory3=cory;
        
        if(cory==cory2)
            System.out.println("equal");
        else
            System.out.println("not equal");
        
        if(cory2==cory3)
            System.out.println("equal");
        else
            System.out.println("not equal");
        
        if(cory==cory3)
            System.out.println("equal");
        else
            System.out.println("not equal");
        
        if(cory==cory4)
            System.out.println("equal");
        else
            System.out.println("not equal");
        
        if(cory.equals(cory4))
            System.out.println("equal");
        else
            System.out.println("not equal");
        
        
    }

}
