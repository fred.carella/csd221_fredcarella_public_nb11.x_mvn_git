/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex3;

import __w21.lecture5.ex3.controllers.DieselJpaController;
import __w21.lecture5.ex3.controllers.GasolineJpaController;
import __w21.lecture5.ex3.controllers.VehicleJpaController;
import __w21.lecture5.ex3.entities.Diesel;
import __w21.lecture5.ex3.entities.Gasoline;
import __w21.lecture5.ex3.entities.Vehicle;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author students
 */
public class App {

    void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("abcd_PU");
            em = emf.createEntityManager();
            Logger.getLogger(__w21.lecture5.ex3.Main.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);

            Gasoline car = new Gasoline();
            car.setMake("Ford");
            car.setAtt1("att1-ford");
            Gasoline car2 = new Gasoline();
            car2.setMake("Chevy");
            car2.setAtt1("att1-chevy");
            
            // CRud
            GasolineJpaController gc=new GasolineJpaController(emf);
            DieselJpaController dc = new DieselJpaController(emf);
            gc.create(car);
            gc.create(car2);
            
            // Update
            car2.setMake("subaru");
            gc.edit(car2);
            
            // Delete
            gc.destroy(car2.getId());

            Gasoline car3 = new Gasoline();
            Gasoline car4 = new Gasoline();
            Diesel diesel1 = new Diesel();
            car3.setMake("Toyota");
            car3.setAtt1("att1-toyota");
            gc.create(car3);
            
            car4.setMake("Nissan");
            car4.setAtt1("att1-nissan");
            gc.create(car4);
            
            diesel1.setMake("Mercedes");
            diesel1.setAtt1("att1-mercedes");
            dc.create(diesel1);

            List<Gasoline> gas_cars = gc.findGasolineEntities();
            for(Gasoline gas_car:gas_cars){
                System.out.println("gas car="+gas_car.getMake());
            }
            
            VehicleJpaController vc=new VehicleJpaController(emf);
            List<Vehicle> vehicle_list = vc.findVehicleEntities();
            System.out.println("----------------------------");
            for(Vehicle vehicle:vehicle_list){
                System.out.println("vehicle="+vehicle.getMake());
            }
            

        } catch (Exception e) {
            Logger.getLogger(__w21.lecture5.ex3.Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }

    }

}
