/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex3.controllers;

import __w21.lecture5.ex3.entities.Diesel;
import __w21.lecture5.ex3.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class DieselJpaController implements Serializable {

    public DieselJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Diesel diesel) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(diesel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Diesel diesel) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            diesel = em.merge(diesel);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = diesel.getId();
                if (findDiesel(id) == null) {
                    throw new NonexistentEntityException("The diesel with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Diesel diesel;
            try {
                diesel = em.getReference(Diesel.class, id);
                diesel.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The diesel with id " + id + " no longer exists.", enfe);
            }
            em.remove(diesel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Diesel> findDieselEntities() {
        return findDieselEntities(true, -1, -1);
    }

    public List<Diesel> findDieselEntities(int maxResults, int firstResult) {
        return findDieselEntities(false, maxResults, firstResult);
    }

    private List<Diesel> findDieselEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Diesel as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Diesel findDiesel(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Diesel.class, id);
        } finally {
            em.close();
        }
    }

    public int getDieselCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Diesel as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
