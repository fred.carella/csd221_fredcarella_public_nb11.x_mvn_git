/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex3.controllers;

import __w21.lecture5.ex3.entities.Bikes;
import __w21.lecture5.ex3.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class BikesJpaController implements Serializable {

    public BikesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Bikes bikes) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(bikes);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Bikes bikes) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            bikes = em.merge(bikes);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = bikes.getId();
                if (findBikes(id) == null) {
                    throw new NonexistentEntityException("The bikes with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Bikes bikes;
            try {
                bikes = em.getReference(Bikes.class, id);
                bikes.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bikes with id " + id + " no longer exists.", enfe);
            }
            em.remove(bikes);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Bikes> findBikesEntities() {
        return findBikesEntities(true, -1, -1);
    }

    public List<Bikes> findBikesEntities(int maxResults, int firstResult) {
        return findBikesEntities(false, maxResults, firstResult);
    }

    private List<Bikes> findBikesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Bikes as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Bikes findBikes(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Bikes.class, id);
        } finally {
            em.close();
        }
    }

    public int getBikesCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Bikes as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
