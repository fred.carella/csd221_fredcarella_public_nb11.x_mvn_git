/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex3.controllers;

import __w21.lecture5.ex3.entities.Gasoline;
import __w21.lecture5.ex3.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class GasolineJpaController implements Serializable {

    public GasolineJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Gasoline gasoline) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(gasoline);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Gasoline gasoline) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            gasoline = em.merge(gasoline);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = gasoline.getId();
                if (findGasoline(id) == null) {
                    throw new NonexistentEntityException("The gasoline with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Gasoline gasoline;
            try {
                gasoline = em.getReference(Gasoline.class, id);
                gasoline.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The gasoline with id " + id + " no longer exists.", enfe);
            }
            em.remove(gasoline);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Gasoline> findGasolineEntities() {
        return findGasolineEntities(true, -1, -1);
    }

    public List<Gasoline> findGasolineEntities(int maxResults, int firstResult) {
        return findGasolineEntities(false, maxResults, firstResult);
    }

    private List<Gasoline> findGasolineEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Gasoline as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Gasoline findGasoline(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Gasoline.class, id);
        } finally {
            em.close();
        }
    }

    public int getGasolineCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Gasoline as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
