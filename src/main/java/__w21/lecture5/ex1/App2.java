/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex1;

import __w21.lecture5.ex1.entities2.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author students
 */
public class App2 {

    void run() {

        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("lab5ex_w21_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);
            em.getTransaction().begin();
            
            Book21 book=new Book21();
            book.setAuthor("Fred");
            book.setCopies(10);
            book.setTitle("Java is Awesome!");
            em.persist(book);
            
            Magazine21 mag=new Magazine21();
            mag.setCopies(15);
            mag.setCurrIssue("today");
            mag.setOrderQty(12);
            em.persist(mag);

            Ticket21 t=new Ticket21();
            t.setDescription("hello");
            em.persist(t);
            em.getTransaction().commit();

        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }

    }

}
