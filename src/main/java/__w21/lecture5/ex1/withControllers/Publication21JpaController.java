/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex1.withControllers;

import __w21.lecture5.ex1.entities2.Publication21;
import __w21.lecture5.ex1.withControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Publication21JpaController implements Serializable {

    public Publication21JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Publication21 publication21) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(publication21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Publication21 publication21) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            publication21 = em.merge(publication21);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = publication21.getId();
                if (findPublication21(id) == null) {
                    throw new NonexistentEntityException("The publication21 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Publication21 publication21;
            try {
                publication21 = em.getReference(Publication21.class, id);
                publication21.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The publication21 with id " + id + " no longer exists.", enfe);
            }
            em.remove(publication21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Publication21> findPublication21Entities() {
        return findPublication21Entities(true, -1, -1);
    }

    public List<Publication21> findPublication21Entities(int maxResults, int firstResult) {
        return findPublication21Entities(false, maxResults, firstResult);
    }

    private List<Publication21> findPublication21Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Publication21 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Publication21 findPublication21(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Publication21.class, id);
        } finally {
            em.close();
        }
    }

    public int getPublication21Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Publication21 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
