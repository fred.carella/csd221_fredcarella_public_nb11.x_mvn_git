/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex1.withControllers;

import __w21.lecture5.ex1.entities2.DiscMag21;
import __w21.lecture5.ex1.withControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class DiscMag21JpaController implements Serializable {

    public DiscMag21JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DiscMag21 discMag21) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(discMag21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DiscMag21 discMag21) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            discMag21 = em.merge(discMag21);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = discMag21.getId();
                if (findDiscMag21(id) == null) {
                    throw new NonexistentEntityException("The discMag21 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DiscMag21 discMag21;
            try {
                discMag21 = em.getReference(DiscMag21.class, id);
                discMag21.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The discMag21 with id " + id + " no longer exists.", enfe);
            }
            em.remove(discMag21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DiscMag21> findDiscMag21Entities() {
        return findDiscMag21Entities(true, -1, -1);
    }

    public List<DiscMag21> findDiscMag21Entities(int maxResults, int firstResult) {
        return findDiscMag21Entities(false, maxResults, firstResult);
    }

    private List<DiscMag21> findDiscMag21Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from DiscMag21 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DiscMag21 findDiscMag21(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DiscMag21.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiscMag21Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from DiscMag21 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
