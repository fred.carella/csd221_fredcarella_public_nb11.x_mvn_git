/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex1.withControllers;

import __w21.lecture5.ex1.entities2.Book21;
import __w21.lecture5.ex1.withControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Book21JpaController implements Serializable {

    public Book21JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Book21 book21) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(book21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Book21 book21) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            book21 = em.merge(book21);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = book21.getId();
                if (findBook21(id) == null) {
                    throw new NonexistentEntityException("The book21 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Book21 book21;
            try {
                book21 = em.getReference(Book21.class, id);
                book21.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The book21 with id " + id + " no longer exists.", enfe);
            }
            em.remove(book21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Book21> findBook21Entities() {
        return findBook21Entities(true, -1, -1);
    }

    public List<Book21> findBook21Entities(int maxResults, int firstResult) {
        return findBook21Entities(false, maxResults, firstResult);
    }

    private List<Book21> findBook21Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Book21 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Book21 findBook21(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Book21.class, id);
        } finally {
            em.close();
        }
    }

    public int getBook21Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Book21 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
