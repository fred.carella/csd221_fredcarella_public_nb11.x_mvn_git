/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex1.withControllers;

import __w21.lecture5.ex1.entities2.Ticket21;
import __w21.lecture5.ex1.withControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Ticket21JpaController implements Serializable {

    public Ticket21JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ticket21 ticket21) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ticket21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ticket21 ticket21) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ticket21 = em.merge(ticket21);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = ticket21.getId();
                if (findTicket21(id) == null) {
                    throw new NonexistentEntityException("The ticket21 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ticket21 ticket21;
            try {
                ticket21 = em.getReference(Ticket21.class, id);
                ticket21.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ticket21 with id " + id + " no longer exists.", enfe);
            }
            em.remove(ticket21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ticket21> findTicket21Entities() {
        return findTicket21Entities(true, -1, -1);
    }

    public List<Ticket21> findTicket21Entities(int maxResults, int firstResult) {
        return findTicket21Entities(false, maxResults, firstResult);
    }

    private List<Ticket21> findTicket21Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Ticket21 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ticket21 findTicket21(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ticket21.class, id);
        } finally {
            em.close();
        }
    }

    public int getTicket21Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Ticket21 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
