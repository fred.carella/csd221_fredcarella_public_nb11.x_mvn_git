/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex1.withControllers;

import __w21.lecture5.ex1.entities2.Book21;
import __w21.lecture5.ex1.entities2.Magazine21;
import __w21.lecture5.ex1.entities2.Ticket21;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author students
 */
public class App {

    void run() {

        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("lab5ex_w21_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);
            
            Book21 book=new Book21();
            book.setAuthor("Fred");
            book.setCopies(10);
            book.setTitle("Java is Awesome!");
            
            Magazine21 mag=new Magazine21();
            mag.setCopies(15);
            mag.setCurrIssue("today");
            mag.setOrderQty(12);

            Ticket21 t=new Ticket21();
            t.setDescription("hello");

            Book21JpaController bookController=new Book21JpaController(emf);
            Magazine21JpaController magController=new Magazine21JpaController(emf);
            Ticket21JpaController ticketController=new Ticket21JpaController(emf);
            // CReate
            bookController.create(book);
            magController.create(mag);
            ticketController.create(t);
            //Update
            book.setAuthor("FredEdited");
            book.setCopies(20);
            book.setTitle("Java is AwesomeEdited!");
            bookController.edit(book);
            
            mag.setCopies(20);
            mag.setCurrIssue("todayEdited");
            mag.setOrderQty(20);
            magController.edit(mag);
            
            t.setDescription("helloEdited");
            ticketController.edit(t);

        } catch (Exception e) {
            Logger.getLogger(__w21.lecture5.ex1.Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }

    }

}
