/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex1.withControllers;

import __w21.lecture5.ex1.entities2.Magazine21;
import __w21.lecture5.ex1.withControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Magazine21JpaController implements Serializable {

    public Magazine21JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Magazine21 magazine21) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(magazine21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Magazine21 magazine21) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            magazine21 = em.merge(magazine21);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = magazine21.getId();
                if (findMagazine21(id) == null) {
                    throw new NonexistentEntityException("The magazine21 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Magazine21 magazine21;
            try {
                magazine21 = em.getReference(Magazine21.class, id);
                magazine21.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The magazine21 with id " + id + " no longer exists.", enfe);
            }
            em.remove(magazine21);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Magazine21> findMagazine21Entities() {
        return findMagazine21Entities(true, -1, -1);
    }

    public List<Magazine21> findMagazine21Entities(int maxResults, int firstResult) {
        return findMagazine21Entities(false, maxResults, firstResult);
    }

    private List<Magazine21> findMagazine21Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Magazine21 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Magazine21 findMagazine21(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Magazine21.class, id);
        } finally {
            em.close();
        }
    }

    public int getMagazine21Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Magazine21 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
