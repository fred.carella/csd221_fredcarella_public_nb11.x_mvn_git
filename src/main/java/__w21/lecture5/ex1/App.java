/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture5.ex1;

import __w21.lecture5.ex1.entities.SubA;
import __w21.lecture5.ex1.entities.SubB;
import __w21.lecture5.ex1.entities.SubSubA;
import __w21.lecture5.ex1.entities.SuperA;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author student
 */
public class App {

    void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("L5_W21_DEFAULT_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);
            em.getTransaction().begin();
            SubA subA = new SubA();
            subA.setAtt1("att1");
            SubB subB = new SubB();
            subB.setAtt1("att1");

            SubSubA subSubA = new SubSubA();
            subSubA.setAtt1("att1");
            subSubA.setAtt2("att2");


            em.persist(subA);
            em.persist(subB);
            em.persist(subSubA);
            em.getTransaction().commit();

            List<SuperA> ListOfSuperAs = em.createQuery("SELECT c FROM SuperA c").getResultList();
            System.out.println("List of SuperAs");
            for (SuperA superAs : ListOfSuperAs) {
                System.out.println(superAs.getId());
            }
            
            List<SubA> ListOfSubAs = em.createQuery("SELECT c FROM SubA c").getResultList();
            System.out.println("List of SubAs");
            for (SubA subAs : ListOfSubAs) {
                System.out.println(subAs.getAtt1());
            }
            List<SubSubA> ListOfSubSubAs = em.createQuery("SELECT c FROM SubSubA c").getResultList();
            System.out.println("List of SubSubAs");
            for (SubSubA subSubAs : ListOfSubSubAs) {
                System.out.println(subSubAs.getAtt2());
            }

        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }

}
