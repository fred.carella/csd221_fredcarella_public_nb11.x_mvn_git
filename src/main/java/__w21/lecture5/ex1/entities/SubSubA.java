package __w21.lecture5.ex1.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author students
 */
@Entity
public class SubSubA extends SubA {

    @Basic
    private String att2;

    public String getAtt2() {
        return att2;
    }

    public void setAtt2(String att2) {
        this.att2 = att2;
    }

}