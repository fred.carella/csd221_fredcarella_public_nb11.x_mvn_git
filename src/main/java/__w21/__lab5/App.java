/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__lab5;

import __w21.__lab5.entities.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author students
 */
public class App {

    public void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("lab5_tmp_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created (" + emf + ")");
            em.getTransaction().begin();
            
            Book__lab5 book=new Book__lab5();
            book.setAuthor("Me");
            book.setTitle("My Book");
            book.setCopies(10);
            book.setPrice(12.34);
            
            Magazine__lab5 mag=new Magazine__lab5();
            mag.setTitle("Mag");
            mag.setPrice(56.89);
            
            em.persist(book);
            em.persist(mag);

            em.getTransaction().commit();

            List<Publication__lab5> ListOfPublications = em.createQuery("SELECT c FROM Publication__lab5 c").getResultList();
            System.out.println("List of Publications");
            for (Publication__lab5 pub : ListOfPublications) {
                System.out.println(pub.getTitle());
            }
            
            em.getTransaction().begin();
            book.setTitle("Beginners Guide to Java");
            em.getTransaction().commit();
            
            em.getTransaction().begin();
            em.remove(book);
            em.getTransaction().commit();


        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }
}
