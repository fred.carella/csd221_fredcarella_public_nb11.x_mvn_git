/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__lab5.controllers;

import __w21.__lab5.controllers.exceptions.NonexistentEntityException;
import __w21.__lab5.entities.Magazine__lab5;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Magazine__lab5JpaController implements Serializable {

    public Magazine__lab5JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Magazine__lab5 magazine__lab5) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(magazine__lab5);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Magazine__lab5 magazine__lab5) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            magazine__lab5 = em.merge(magazine__lab5);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = magazine__lab5.getId();
                if (findMagazine__lab5(id) == null) {
                    throw new NonexistentEntityException("The magazine__lab5 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Magazine__lab5 magazine__lab5;
            try {
                magazine__lab5 = em.getReference(Magazine__lab5.class, id);
                magazine__lab5.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The magazine__lab5 with id " + id + " no longer exists.", enfe);
            }
            em.remove(magazine__lab5);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Magazine__lab5> findMagazine__lab5Entities() {
        return findMagazine__lab5Entities(true, -1, -1);
    }

    public List<Magazine__lab5> findMagazine__lab5Entities(int maxResults, int firstResult) {
        return findMagazine__lab5Entities(false, maxResults, firstResult);
    }

    private List<Magazine__lab5> findMagazine__lab5Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Magazine__lab5 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Magazine__lab5 findMagazine__lab5(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Magazine__lab5.class, id);
        } finally {
            em.close();
        }
    }

    public int getMagazine__lab5Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Magazine__lab5 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
