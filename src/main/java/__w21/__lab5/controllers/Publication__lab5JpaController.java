/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__lab5.controllers;

import __w21.__lab5.controllers.exceptions.NonexistentEntityException;
import __w21.__lab5.entities.Publication__lab5;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Publication__lab5JpaController implements Serializable {

    public Publication__lab5JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Publication__lab5 publication__lab5) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(publication__lab5);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Publication__lab5 publication__lab5) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            publication__lab5 = em.merge(publication__lab5);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = publication__lab5.getId();
                if (findPublication__lab5(id) == null) {
                    throw new NonexistentEntityException("The publication__lab5 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Publication__lab5 publication__lab5;
            try {
                publication__lab5 = em.getReference(Publication__lab5.class, id);
                publication__lab5.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The publication__lab5 with id " + id + " no longer exists.", enfe);
            }
            em.remove(publication__lab5);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Publication__lab5> findPublication__lab5Entities() {
        return findPublication__lab5Entities(true, -1, -1);
    }

    public List<Publication__lab5> findPublication__lab5Entities(int maxResults, int firstResult) {
        return findPublication__lab5Entities(false, maxResults, firstResult);
    }

    private List<Publication__lab5> findPublication__lab5Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Publication__lab5 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Publication__lab5 findPublication__lab5(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Publication__lab5.class, id);
        } finally {
            em.close();
        }
    }

    public int getPublication__lab5Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Publication__lab5 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
