/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__lab5.controllers;

import __w21.__lab5.controllers.exceptions.NonexistentEntityException;
import __w21.__lab5.entities.Book__lab5;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Book__lab5JpaController implements Serializable {

    public Book__lab5JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Book__lab5 book__lab5) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(book__lab5);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Book__lab5 book__lab5) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            book__lab5 = em.merge(book__lab5);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = book__lab5.getId();
                if (findBook__lab5(id) == null) {
                    throw new NonexistentEntityException("The book__lab5 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Book__lab5 book__lab5;
            try {
                book__lab5 = em.getReference(Book__lab5.class, id);
                book__lab5.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The book__lab5 with id " + id + " no longer exists.", enfe);
            }
            em.remove(book__lab5);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Book__lab5> findBook__lab5Entities() {
        return findBook__lab5Entities(true, -1, -1);
    }

    public List<Book__lab5> findBook__lab5Entities(int maxResults, int firstResult) {
        return findBook__lab5Entities(false, maxResults, firstResult);
    }

    private List<Book__lab5> findBook__lab5Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Book__lab5 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Book__lab5 findBook__lab5(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Book__lab5.class, id);
        } finally {
            em.close();
        }
    }

    public int getBook__lab5Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Book__lab5 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
