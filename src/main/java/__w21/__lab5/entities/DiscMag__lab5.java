package __w21.__lab5.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author students
 */

@Entity
public class DiscMag__lab5 extends Magazine__lab5 implements Serializable {

    @Basic
    private boolean hasDisk;

    public boolean isHasDisk() {
        return hasDisk;
    }

    public void setHasDisk(boolean hasDisk) {
        this.hasDisk = hasDisk;
    }

}