package __w21.__lab5.entities;

import __w21.__lab5.controllers.Book__lab5JpaController;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author students
 */
@Entity
public class Magazine__lab5 extends Publication__lab5 implements Serializable {

    @Basic
    private int orderQty;
    @Basic
    private String currIssue;

    public int getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    public String getCurrIssue() {
        return currIssue;
    }

    public void setCurrIssue(String currIssue) {
        this.currIssue = currIssue;
    }

    @Override
    public void sellCopy() {
    }
}
