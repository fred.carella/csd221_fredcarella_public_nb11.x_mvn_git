package __w21.__lab5.entities;

import __w21.__lab5.controllers.Book__lab5JpaController;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author students
 */

@Entity
public class Book__lab5 extends Publication__lab5 implements Serializable {

    @Basic
    private String author;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public void sellCopy() {
        EntityManagerFactory emf = null;
        System.out.println("Selling Magazine");
        setCopies(getCopies() - 1);
        emf = Persistence.createEntityManagerFactory("lab5_tmp_PU");
        Book__lab5JpaController bc = new Book__lab5JpaController(emf);
        try {
            bc.edit(this);
        } catch (Exception ex) {
            Logger.getLogger(Book__lab5.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}