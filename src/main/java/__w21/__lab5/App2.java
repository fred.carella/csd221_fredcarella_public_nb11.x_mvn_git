/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__lab5;

import __w21.__lab5.entities.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import __w21.__lab5.controllers.*;
/**
 *
 * @author students
 */
public class App2 {

    public void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("lab5_tmp_PU");
//            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created (" + emf + ")");

            Book__lab5 book=new Book__lab5();
            book.setAuthor("Me");
            book.setTitle("My Book");
            book.setCopies(10);
            book.setPrice(12.34);
            
            Magazine__lab5 mag=new Magazine__lab5();
            mag.setTitle("Mag");
            mag.setPrice(56.89);
            
            Book__lab5JpaController bookController=new Book__lab5JpaController(emf);
            bookController.create(book);
            
            Magazine__lab5JpaController magController=new Magazine__lab5JpaController(emf);
            magController.create(mag);
            
            book.setTitle("New Title");
            bookController.edit(book);
            
            List<Book__lab5> ListOfBooks = bookController.findBook__lab5Entities();
            System.out.println("List of Books");
            for (Publication__lab5 pub : ListOfBooks) {
                System.out.println(""+pub.getTitle()+" id="+pub.getId());
            }
            
            
            

        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }
}
