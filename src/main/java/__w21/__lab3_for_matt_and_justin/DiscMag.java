/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__lab3_for_matt_and_justin;

/**
 *
 * @author students
 */
public class DiscMag extends Magazine {
    private boolean hasDisc;

    @Override
    public void receiveNewIssue(String date) {
        super.receiveNewIssue(date); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Magazine has a disc");
    }

    public DiscMag() {
        hasDisc=true;
    }

    /**
     * @return the hasDisc
     */
    public boolean isHasDisc() {
        return hasDisc;
    }

    /**
     * @param hasDisc the hasDisc to set
     */
    public void setHasDisc(boolean hasDisc) {
        this.hasDisc = hasDisc;
    }
    
    
    
}
