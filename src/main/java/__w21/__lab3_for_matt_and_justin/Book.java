/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__lab3_for_matt_and_justin;

/**
 *
 * @author students
 */
public class Book extends Publication{
    private String author;

    public Book() {
        author="no author";
    }

    public Book(String author) {
        this.author = author;
    }
    public Book(String author, String title, int copies, double price) {
        this.author = author;
        setTitle(title);
        setCopies(copies);
        setPrice(price);
    }
    
    

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }
    
    public void orderCopies(int copies){
        setCopies(copies);
    }
    

    @Override
    public String toString() {
        return super.toString()+", Author: "+author; //To change body of generated methods, choose Tools | Templates.
    }
    
    

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }
    
    
    
}
