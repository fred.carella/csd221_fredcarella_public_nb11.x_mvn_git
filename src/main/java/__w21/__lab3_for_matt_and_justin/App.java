/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__lab3_for_matt_and_justin;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author students
 */
public class App {
    private List<Book> bookList=new ArrayList<>();
    private List<Magazine> magList=new ArrayList<>();
    private List<DiscMag> dMagList=new ArrayList<>();
    
    private List<Publication> publicationList=new ArrayList<>();
    
    
    public void run(){
        Book book=new Book();
        Book book2=new Book("Matther Chilleli & Justin Wagner");
        Book book3=new Book("Matther Chilleli & Justin Wagner", "I Love Lab 3", 12, 12.34);
        
        Magazine mag=new Magazine(13, "31-mar-2021");
        
        DiscMag dMag=new DiscMag();
        dMag.setHasDisc(false);
        
        
        bookList.add(book);
        bookList.add(book2);
        bookList.add(book3);
        
        magList.add(mag);
        
        dMagList.add(dMag);
        
        publicationList.add(book);
        publicationList.add(book2);
        publicationList.add(book3);
        publicationList.add(mag);
        publicationList.add(dMag);
        
        int n=0;
        for(Book b:bookList){
            n++;
            System.out.println(n+". book="+b);
        }
        n=0;
        for(Magazine m:magList){
            n++;
            System.out.println(n+". mag="+m);
        }
        
        n=0;
        for(Publication p:publicationList){
            n++;
            System.out.println(n+". publication="+p);
            if(p instanceof Book)
                System.out.println("p is a book"+p);
        }
        
//        System.out.println("book="+book);
//        System.out.println("book2="+book2);
//        System.out.println("book3="+book3);
//        System.out.println("mag="+mag);
//        System.out.println("dMag="+dMag);
    }
}
