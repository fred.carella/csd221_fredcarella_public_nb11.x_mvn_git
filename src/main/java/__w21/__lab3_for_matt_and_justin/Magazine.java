/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.__lab3_for_matt_and_justin;

/**
 *
 * @author students
 */
public class Magazine extends Publication {
    private int orderQty;
    private String curIssue;

    public Magazine() {
        orderQty=10;
        curIssue="now";
    }

    public Magazine(int orderQty, String curIssue) {
        this.orderQty = orderQty;
        this.curIssue = curIssue;
    }

    public void adjustQty(int qty){
        orderQty+=qty;
    }
    
    public void receiveNewIssue(String date){
        curIssue=date;
    }

    @Override
    public String toString() {
        return super.toString()+" Current Issue: "+curIssue+" Order Qty: "+orderQty; 
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    
    /**
     * @return the orderQty
     */
    public int getOrderQty() {
        return orderQty;
    }

    /**
     * @param orderQty the orderQty to set
     */
    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * @return the curIssue
     */
    public String getCurIssue() {
        return curIssue;
    }

    /**
     * @param curIssue the curIssue to set
     */
    public void setCurIssue(String curIssue) {
        this.curIssue = curIssue;
    }
    
    
}
