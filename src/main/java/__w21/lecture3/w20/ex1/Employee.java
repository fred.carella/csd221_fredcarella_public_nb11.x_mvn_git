/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __w21.lecture3.w20.ex1;

/**
 *
 * @author student
 */
public class Employee extends Person {
    private String employer;

    public Employee() {
        System.out.println("calling Employee() constructor");
        employer="<default employer>";
    }

    public Employee(String firstname, String lastname, String employer) {
        super(firstname, lastname);
        System.out.println("calling Employee(String firstname, String lastname, String employer) constructor");
        this.employer = employer;
    }

    public Employee(String employer) {
        System.out.println("calling Employee(String employer) constructor");
        this.employer = employer;
    }

    /**
     * @return the employer
     */
    public String getEmployer() {
        return employer;
    }

    /**
     * @param employer the employer to set
     */
    public void setEmployer(String employer) {
        this.employer = employer;
    }

    @Override
    public String toString() {
        return super.toString()+" : "+employer;
    }
    
    
    
}
