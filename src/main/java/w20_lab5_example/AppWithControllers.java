/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20_lab5_example;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import w20_lab5_example.controllers.BookJpaController;
import w20_lab5_example.entities.*;

/**
 *
 * @author student
 */
public class AppWithControllers {

    void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        emf = Persistence.createEntityManagerFactory("W20_LAB5_EXAMPLE_PU");
        em = emf.createEntityManager();
        Logger.getLogger(App.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);

        Book book1 = new Book();
        book1.setAuthor("Fred Carella");
        book1.setCopies(5);
        book1.setPrice(34.15);
        book1.setTitle("I love jpa");

        BookJpaController bookController = new BookJpaController(emf);
        bookController.create(book1);
        
        List<Book> books = bookController.findBookEntities();
        for(Book b:books){
            System.out.println("Book="+b.getTitle());
        }
        

    }

}
