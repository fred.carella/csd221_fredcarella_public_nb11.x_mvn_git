package w20_lab5_example.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author student
 */

@Entity
public class DiscMagazine extends Magazine {

    @Basic
    private boolean hasDisk;

    public boolean isHasDisk() {
        return hasDisk;
    }

    public void setHasDisk(boolean hasDisk) {
        this.hasDisk = hasDisk;
    }

}