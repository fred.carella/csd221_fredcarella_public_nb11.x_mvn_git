/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20_jpa_review.controllers;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import w20_jpa_review.controllers.exceptions.NonexistentEntityException;
import w20_jpa_review.entities.SuperClass1;

/**
 *
 * @author student
 */
public class SuperClass1JpaController implements Serializable {

    public SuperClass1JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SuperClass1 superClass1) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(superClass1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SuperClass1 superClass1) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            superClass1 = em.merge(superClass1);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = superClass1.getId();
                if (findSuperClass1(id) == null) {
                    throw new NonexistentEntityException("The superClass1 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SuperClass1 superClass1;
            try {
                superClass1 = em.getReference(SuperClass1.class, id);
                superClass1.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The superClass1 with id " + id + " no longer exists.", enfe);
            }
            em.remove(superClass1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SuperClass1> findSuperClass1Entities() {
        return findSuperClass1Entities(true, -1, -1);
    }

    public List<SuperClass1> findSuperClass1Entities(int maxResults, int firstResult) {
        return findSuperClass1Entities(false, maxResults, firstResult);
    }

    private List<SuperClass1> findSuperClass1Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from SuperClass1 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SuperClass1 findSuperClass1(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SuperClass1.class, id);
        } finally {
            em.close();
        }
    }

    public int getSuperClass1Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from SuperClass1 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
