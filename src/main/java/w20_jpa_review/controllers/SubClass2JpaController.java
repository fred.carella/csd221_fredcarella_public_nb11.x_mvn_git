/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20_jpa_review.controllers;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import w20_jpa_review.controllers.exceptions.NonexistentEntityException;
import w20_jpa_review.entities.SubClass2;

/**
 *
 * @author student
 */
public class SubClass2JpaController implements Serializable {

    public SubClass2JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SubClass2 subClass2) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(subClass2);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SubClass2 subClass2) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            subClass2 = em.merge(subClass2);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = subClass2.getId();
                if (findSubClass2(id) == null) {
                    throw new NonexistentEntityException("The subClass2 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SubClass2 subClass2;
            try {
                subClass2 = em.getReference(SubClass2.class, id);
                subClass2.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The subClass2 with id " + id + " no longer exists.", enfe);
            }
            em.remove(subClass2);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SubClass2> findSubClass2Entities() {
        return findSubClass2Entities(true, -1, -1);
    }

    public List<SubClass2> findSubClass2Entities(int maxResults, int firstResult) {
        return findSubClass2Entities(false, maxResults, firstResult);
    }

    private List<SubClass2> findSubClass2Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from SubClass2 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SubClass2 findSubClass2(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SubClass2.class, id);
        } finally {
            em.close();
        }
    }

    public int getSubClass2Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from SubClass2 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
