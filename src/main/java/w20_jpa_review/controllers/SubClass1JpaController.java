/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20_jpa_review.controllers;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import w20_jpa_review.controllers.exceptions.NonexistentEntityException;
import w20_jpa_review.entities.SubClass1;

/**
 *
 * @author student
 */
public class SubClass1JpaController implements Serializable {

    public SubClass1JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SubClass1 subClass1) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(subClass1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SubClass1 subClass1) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            subClass1 = em.merge(subClass1);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = subClass1.getId();
                if (findSubClass1(id) == null) {
                    throw new NonexistentEntityException("The subClass1 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SubClass1 subClass1;
            try {
                subClass1 = em.getReference(SubClass1.class, id);
                subClass1.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The subClass1 with id " + id + " no longer exists.", enfe);
            }
            em.remove(subClass1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SubClass1> findSubClass1Entities() {
        return findSubClass1Entities(true, -1, -1);
    }

    public List<SubClass1> findSubClass1Entities(int maxResults, int firstResult) {
        return findSubClass1Entities(false, maxResults, firstResult);
    }

    private List<SubClass1> findSubClass1Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from SubClass1 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SubClass1 findSubClass1(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SubClass1.class, id);
        } finally {
            em.close();
        }
    }

    public int getSubClass1Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from SubClass1 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
