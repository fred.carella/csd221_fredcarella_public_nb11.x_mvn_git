package w20_jpa_review.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author student
 */

@Entity
public class SubClass2 extends SuperClass1 implements Serializable {

    @Basic
    private int uniqueInt2;

    @Basic
    private String uniqueString2;

    public int getUniqueInt2() {
        return uniqueInt2;
    }

    public void setUniqueInt2(int uniqueInt2) {
        this.uniqueInt2 = uniqueInt2;
    }

    public String getUniqueString2() {
        return uniqueString2;
    }

    public void setUniqueString2(String uniqueString2) {
        this.uniqueString2 = uniqueString2;
    }

    @Override
    public void implementMe(int i) {
        // To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }

}