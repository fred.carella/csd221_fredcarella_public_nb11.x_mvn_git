package w20_jpa_review.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author student
 */

@Entity
public class SubClass1 extends SuperClass1 implements Serializable {

    @Basic
    private int uniqueInt1;

    @Basic
    private String uniqueString1;

    public int getUniqueInt1() {
        return uniqueInt1;
    }

    public void setUniqueInt1(int uniqueInt1) {
        this.uniqueInt1 = uniqueInt1;
    }

    public String getUniqueString1() {
        return uniqueString1;
    }

    public void setUniqueString1(String uniqueString1) {
        this.uniqueString1 = uniqueString1;
    }

    @Override
    public void implementMe(int i) {
        // To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }

}