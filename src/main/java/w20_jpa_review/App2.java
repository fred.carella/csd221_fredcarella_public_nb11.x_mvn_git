/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20_jpa_review;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import w20_jpa_review.controllers.SubClass1JpaController;
import w20_jpa_review.entities.SubClass1;

/**
 *
 * @author student
 */
public class App2 {

    public App2() {
    }

    void run() {

        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            emf = Persistence.createEntityManagerFactory("w20_jpa_review_DEFAULT_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created (" + emf + ")");
            
            SubClass1JpaController c=new SubClass1JpaController(emf);
            
            SubClass1 c1=new SubClass1();
            c1.setUniqueInt1(10);
            c1.setUniqueString1("unique 2");
            
            c.create(c1);
            
            List<SubClass1> list=c.findSubClass1Entities();
            
            System.out.println("List 'em");
            for(SubClass1 s1: list)
                System.out.println("object="+s1.getUniqueInt1());

            for(SubClass1 s1: list)
                c.destroy(s1.getId());
            
            list=c.findSubClass1Entities();
            System.out.println("List 'em");
            for(SubClass1 s1: list)
                System.out.println("object="+s1.getUniqueInt1());
            
            
        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }

    }

}
