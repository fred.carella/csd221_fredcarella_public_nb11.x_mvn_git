/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20_jpa_review;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import w20_jpa_review.entities.SubClass1;

/**
 *
 * @author student
 */
public class App {

    public App() {
    }

    void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            emf = Persistence.createEntityManagerFactory("w20_jpa_review_DEFAULT_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created (" + emf + ")");
            em.getTransaction().begin();
            
            SubClass1 c1=new SubClass1();
            c1.setUniqueInt1(1);
            c1.setUniqueString1("Im class 1");
            
            em.persist(c1);
            
            em.getTransaction().commit();
            
            List<SubClass1> ListOfSubClass1 = em.createQuery("SELECT c FROM SubClass1 c").getResultList();
            System.out.println("List of Stuff");
            for(SubClass1 s1:ListOfSubClass1){
                System.out.println(s1.getUniqueInt1());
            }
            
            em.getTransaction().begin();
            for(SubClass1 s1:ListOfSubClass1){
                em.remove(s1);
            }
            em.getTransaction().commit();
            
            ListOfSubClass1 = em.createQuery("SELECT c FROM SubClass1 c").getResultList();
            System.out.println("List of Stuff");
            for(SubClass1 s1:ListOfSubClass1){
                System.out.println(s1.getUniqueInt1());
            }

//            em.getTransaction().begin();
//            Customer c=new Customer();
//            c.setName("Fred");
//            c.setAge(54);
//            c.setAddress(new Address());
//            c.getAddress().setCity("SSM");
//            c.getAddress().setStreet("Ashmun");
//            c.getAddress().setState("MI");
//            em.persist(c);
//            Customer c2=new Customer();
//            c2.setName("Carella");
//            c2.setAge(55);
//            em.persist(c2);
//            Employee e=new Employee();
//            e.setName("FredEmp");
//            e.setAge(54);
//            
//            Department d=new Department();
//            d.setName("fred");
//            em.persist(d);
//            
//            ProductOrder po=new ProductOrder();
//            po.setName("poname");
//            em.persist(po);
//
//            em.persist(e);
//            em.getTransaction().commit();
//            
//            
//            List<Customer> ListOfCustomers = em.createQuery("SELECT c FROM Customer c").getResultList();
//            System.out.println("List of Customers");
//            for(Customer customer:ListOfCustomers){
//                System.out.println(customer.getName());
//            }
        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }

    }

}
