/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _______EricTest;

import _______EricTest.entities.Entity_3;
import _______EricTest.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author fcarella
 */
public class Entity_3JpaController implements Serializable {

    public Entity_3JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Entity_3 entity_3) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(entity_3);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Entity_3 entity_3) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            entity_3 = em.merge(entity_3);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = entity_3.getId();
                if (findEntity_3(id) == null) {
                    throw new NonexistentEntityException("The entity_3 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Entity_3 entity_3;
            try {
                entity_3 = em.getReference(Entity_3.class, id);
                entity_3.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The entity_3 with id " + id + " no longer exists.", enfe);
            }
            em.remove(entity_3);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Entity_3> findEntity_3Entities() {
        return findEntity_3Entities(true, -1, -1);
    }

    public List<Entity_3> findEntity_3Entities(int maxResults, int firstResult) {
        return findEntity_3Entities(false, maxResults, firstResult);
    }

    private List<Entity_3> findEntity_3Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Entity_3 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Entity_3 findEntity_3(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Entity_3.class, id);
        } finally {
            em.close();
        }
    }

    public int getEntity_3Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Entity_3 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
