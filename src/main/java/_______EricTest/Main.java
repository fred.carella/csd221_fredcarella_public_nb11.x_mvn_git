/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _______EricTest;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import _______EricTest.entities.*;
import java.util.List;
/**
 *
 * @author fcarella
 */
public class Main {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        emf = Persistence.createEntityManagerFactory("__ERIC_TEST_DEFAULT_PU");
        Entity_1JpaController e1c = new Entity_1JpaController(emf);
        Entity_2JpaController e2c = new Entity_2JpaController(emf);
        Entity_3JpaController e3c = new Entity_3JpaController(emf);
        
        Entity_1 e1=new Entity_1();
        Entity_2 e2=new Entity_2();
        Entity_3 e3=new Entity_3();
        
        e1.setAttribute("att");
        e2.setAttribute("att2");
        e3.setAttribute("att3");
        
        
        e1c.create(e1);
        e2c.create(e2);
        e3c.create(e3);
        
        Entity_1 e12=new Entity_1("att1");
        Entity_2 e22=new Entity_2("att2", "att1");
        Entity_3 e32=new Entity_3("att2", "att1");
        
        
        
        e1c.create(e12);
        e2c.create(e22);
        e3c.create(e32);
        
        List<Entity_1> list1 = e1c.findEntity_1Entities();
        for(Entity_1 e:list1)
            System.out.println("e="+e);
        
        
        
    }

}
