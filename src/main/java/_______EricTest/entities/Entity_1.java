package _______EricTest.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author fcarella
 */
@Entity
public class Entity_1 implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Basic
    private String attribute;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public Entity_1(String attribute) {
//        this.id = id;
        this.attribute = attribute;
    }

    public Entity_1() {
    }

    @Override
    public String toString() {
        return attribute+": in Entity_1";
    }

    
}