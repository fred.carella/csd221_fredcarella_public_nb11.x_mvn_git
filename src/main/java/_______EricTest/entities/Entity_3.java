package _______EricTest.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author fcarella
 */
@Entity
public class Entity_3 extends Entity_1 {

    @Basic
    private String attribute1;

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public Entity_3(String attribute1, String attribute) {
        super(attribute);
        this.attribute1 = attribute1;
    }

    public Entity_3(String attribute1) {
        this.attribute1 = attribute1;
    }

    public Entity_3() {
    }
    @Override
    public String toString() {
        return super.toString()+": in Entity_3"; //To change body of generated methods, choose Tools | Templates.
    }

}