package w20_test4_practical_review.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author student
 */
@Entity
public class SubClass2 extends SuperClass1 implements Serializable {

    @Basic
    private String uniqueInt2;

    @Basic
    private String uniqueString2;

    public String getUniqueInt2() {
        return uniqueInt2;
    }

    public void setUniqueInt2(String uniqueInt2) {
        this.uniqueInt2 = uniqueInt2;
    }

    public String getUniqueString2() {
        return uniqueString2;
    }

    public void setUniqueString2(String uniqueString2) {
        this.uniqueString2 = uniqueString2;
    }

    @Override
    public void impementMe() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}