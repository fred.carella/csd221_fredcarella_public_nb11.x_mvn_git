package w20_test4_practical_review.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author student
 */
@Entity
public abstract class SuperClass1 implements Interface1 {

    @Id
    @GeneratedValue
    private Long id;

    @Basic
    private String common1;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommon1() {
        return common1;
    }

    public void setCommon1(String common1) {
        this.common1 = common1;
    }

}