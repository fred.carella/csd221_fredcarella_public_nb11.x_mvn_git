package ____w22.lecture5.ex1.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author students
 */
@Entity
public class SubSubA22 extends SubA22 {

    @Basic
    private String att2;

    public String getAtt2() {
        return att2;
    }

    public void setAtt2(String att2) {
        this.att2 = att2;
    }

}