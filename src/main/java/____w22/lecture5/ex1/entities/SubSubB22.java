package ____w22.lecture5.ex1.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author fcarella
 */
@Entity
public class SubSubB22 extends SubB22 {

    @Basic
    private int att3;

    public int getAtt3() {
        return att3;
    }

    public void setAtt3(int att3) {
        this.att3 = att3;
    }

}