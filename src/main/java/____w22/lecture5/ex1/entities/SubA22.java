package ____w22.lecture5.ex1.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author students
 */
@Entity
public class SubA22 extends SuperA22 {

    @Basic
    private String att1;

    public String getAtt1() {
        return att1;
    }

    public void setAtt1(String att1) {
        this.att1 = att1;
    }

}