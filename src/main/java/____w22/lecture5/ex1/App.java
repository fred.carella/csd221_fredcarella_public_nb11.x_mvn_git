/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture5.ex1;

import ____w22.lecture5.ex1.entities.SubA22;
import ____w22.lecture5.ex1.entities.SubB22;
import ____w22.lecture5.ex1.entities.SubSubA22;
import ____w22.lecture5.ex1.entities.SuperA22;
import __w21.lecture5.ex1.entities.SubA;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author fcarella
 */
public class App {

    void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("L5_W22_DEFAULT_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);
            em.getTransaction().begin();
            
            SubA22 subA = new SubA22();
            subA.setAtt1("att1");
            SubB22 subB = new SubB22();
            subB.setAtt1("att1");

            SubSubA22 subSubA = new SubSubA22();
            subSubA.setAtt1("att1");
            subSubA.setAtt2("att2");


            em.persist(subA);
            em.persist(subB);
            em.persist(subSubA);            
            em.getTransaction().commit();
            
            
            List<SuperA22> ListOfSuperAs = em.createQuery("SELECT c FROM SuperA22 c").getResultList();
            System.out.println("List of SuperAs");
            for (SuperA22 superAs : ListOfSuperAs) {
                System.out.println(superAs.getId());
            }
            
            List<SubA22> ListOfSubAs = em.createQuery("SELECT c FROM SubA22 c").getResultList();
            System.out.println("List of SubAs");
            for (SubA22 subAs : ListOfSubAs) {
                System.out.println(subAs.getAtt1());
            }
            List<SubSubA22> ListOfSubSubAs = em.createQuery("SELECT c FROM SubSubA22 c").getResultList();
            System.out.println("List of SubSubAs");
            for (SubSubA22 subSubAs : ListOfSubSubAs) {
                System.out.println(subSubAs.getAtt2());
            }
            } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }
    
}
