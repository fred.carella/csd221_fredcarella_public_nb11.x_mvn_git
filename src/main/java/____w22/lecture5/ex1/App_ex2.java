/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture5.ex1;

import ____w22.lecture5.ex1.entities2.Book22;
import ____w22.lecture5.ex1.entities2.Magazine22;
import ____w22.lecture5.ex1.entities2.Ticket22;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author fcarella
 */
public class App_ex2 {

 void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("lab5ex2_w22_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);
            em.getTransaction().begin();
            
            Book22 book=new Book22();
            book.setAuthor("Fred");
            book.setCopies(10);
            book.setTitle("Java is Awesome!");
            em.persist(book);
            
            Magazine22 mag=new Magazine22();
            mag.setCopies(15);
            mag.setCurrIssue("today");
            mag.setOrderQty(12);
            em.persist(mag);

            Ticket22 t=new Ticket22();
            t.setDescription("hello");
            em.persist(t);
            em.getTransaction().commit();
            } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }    
}
