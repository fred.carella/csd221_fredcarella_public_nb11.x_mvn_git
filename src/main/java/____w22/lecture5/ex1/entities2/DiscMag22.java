package ____w22.lecture5.ex1.entities2;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author students
 */
@Entity
public class DiscMag22 extends Magazine22 implements Serializable {

    @Basic
    private boolean disc;

    public boolean isDisc() {
        return disc;
    }

    public void setDisc(boolean disc) {
        this.disc = disc;
    }

}