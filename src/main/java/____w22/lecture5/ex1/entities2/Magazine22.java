package ____w22.lecture5.ex1.entities2;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author students
 */
@Entity
public class Magazine22 extends Publication22 implements Serializable {

    @Basic
    private int orderQty;
    @Basic
    private String currIssue;

    public int getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    public String getCurrIssue() {
        return currIssue;
    }

    public void setCurrIssue(String currIssue) {
        this.currIssue = currIssue;
    }

}