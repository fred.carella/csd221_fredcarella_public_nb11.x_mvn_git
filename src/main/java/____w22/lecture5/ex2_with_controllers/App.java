/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture5.ex2_with_controllers;

import ____w22.lecture5.ex1.entities2.Book22;
import ____w22.lecture5.ex1.entities2.Magazine22;
import ____w22.lecture5.ex1.entities2.Ticket22;
import ____w22.lecture5.ex2_with_controllers.controllers.Book22JpaController;
import ____w22.lecture5.ex2_with_controllers.controllers.Magazine22JpaController;
import ____w22.lecture5.ex2_with_controllers.controllers.Ticket22JpaController;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author fcarella
 */
public class App {

    void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("lab5ex2_w22_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);

            Book22 book = new Book22();
            book.setAuthor("Fred");
            book.setCopies(10);
            book.setTitle("Java is Awesome!");

            Magazine22 mag = new Magazine22();
            mag.setCopies(15);
            mag.setCurrIssue("today");
            mag.setOrderQty(12);

            Ticket22 t = new Ticket22();
            t.setDescription("hello");

            Book22JpaController bookController = new Book22JpaController(emf);
            Magazine22JpaController magController = new Magazine22JpaController(emf);
            Ticket22JpaController ticketController = new Ticket22JpaController(emf);
            // CReate
            bookController.create(book);
            magController.create(mag);
            ticketController.create(t);
            //Update
            book.setAuthor("FredEdited");
            book.setCopies(20);
            book.setTitle("Java is AwesomeEdited!");
            bookController.edit(book);

            mag.setCopies(20);
            mag.setCurrIssue("todayEdited");
            mag.setOrderQty(20);
            magController.edit(mag);

            t.setDescription("helloEdited");
            ticketController.edit(t);
            
            bookController.destroy(book.getId());

        } catch (Exception ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }

}
