/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture5.ex2_with_controllers.controllers;

import ____w22.lecture5.ex1.entities2.DiscMag22;
import ____w22.lecture5.ex2_with_controllers.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author fcarella
 */
public class DiscMag22JpaController implements Serializable {

    public DiscMag22JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DiscMag22 discMag22) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(discMag22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DiscMag22 discMag22) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            discMag22 = em.merge(discMag22);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = discMag22.getId();
                if (findDiscMag22(id) == null) {
                    throw new NonexistentEntityException("The discMag22 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DiscMag22 discMag22;
            try {
                discMag22 = em.getReference(DiscMag22.class, id);
                discMag22.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The discMag22 with id " + id + " no longer exists.", enfe);
            }
            em.remove(discMag22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DiscMag22> findDiscMag22Entities() {
        return findDiscMag22Entities(true, -1, -1);
    }

    public List<DiscMag22> findDiscMag22Entities(int maxResults, int firstResult) {
        return findDiscMag22Entities(false, maxResults, firstResult);
    }

    private List<DiscMag22> findDiscMag22Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from DiscMag22 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DiscMag22 findDiscMag22(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DiscMag22.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiscMag22Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from DiscMag22 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
