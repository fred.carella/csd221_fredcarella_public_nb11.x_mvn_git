/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture5.ex2_with_controllers.controllers;

import ____w22.lecture5.ex1.entities2.Ticket22;
import ____w22.lecture5.ex2_with_controllers.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author fcarella
 */
public class Ticket22JpaController implements Serializable {

    public Ticket22JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ticket22 ticket22) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ticket22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ticket22 ticket22) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ticket22 = em.merge(ticket22);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = ticket22.getId();
                if (findTicket22(id) == null) {
                    throw new NonexistentEntityException("The ticket22 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ticket22 ticket22;
            try {
                ticket22 = em.getReference(Ticket22.class, id);
                ticket22.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ticket22 with id " + id + " no longer exists.", enfe);
            }
            em.remove(ticket22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ticket22> findTicket22Entities() {
        return findTicket22Entities(true, -1, -1);
    }

    public List<Ticket22> findTicket22Entities(int maxResults, int firstResult) {
        return findTicket22Entities(false, maxResults, firstResult);
    }

    private List<Ticket22> findTicket22Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Ticket22 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ticket22 findTicket22(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ticket22.class, id);
        } finally {
            em.close();
        }
    }

    public int getTicket22Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Ticket22 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
