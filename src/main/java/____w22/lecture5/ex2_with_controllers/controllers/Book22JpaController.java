/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture5.ex2_with_controllers.controllers;

import ____w22.lecture5.ex1.entities2.Book22;
import ____w22.lecture5.ex2_with_controllers.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author fcarella
 */
public class Book22JpaController implements Serializable {

    public Book22JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Book22 book22) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(book22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Book22 book22) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            book22 = em.merge(book22);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = book22.getId();
                if (findBook22(id) == null) {
                    throw new NonexistentEntityException("The book22 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Book22 book22;
            try {
                book22 = em.getReference(Book22.class, id);
                book22.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The book22 with id " + id + " no longer exists.", enfe);
            }
            em.remove(book22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Book22> findBook22Entities() {
        return findBook22Entities(true, -1, -1);
    }

    public List<Book22> findBook22Entities(int maxResults, int firstResult) {
        return findBook22Entities(false, maxResults, firstResult);
    }

    private List<Book22> findBook22Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Book22 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Book22 findBook22(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Book22.class, id);
        } finally {
            em.close();
        }
    }

    public int getBook22Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Book22 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
