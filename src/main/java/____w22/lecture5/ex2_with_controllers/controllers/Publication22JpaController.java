/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture5.ex2_with_controllers.controllers;

import ____w22.lecture5.ex1.entities2.Publication22;
import ____w22.lecture5.ex2_with_controllers.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author fcarella
 */
public class Publication22JpaController implements Serializable {

    public Publication22JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Publication22 publication22) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(publication22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Publication22 publication22) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            publication22 = em.merge(publication22);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = publication22.getId();
                if (findPublication22(id) == null) {
                    throw new NonexistentEntityException("The publication22 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Publication22 publication22;
            try {
                publication22 = em.getReference(Publication22.class, id);
                publication22.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The publication22 with id " + id + " no longer exists.", enfe);
            }
            em.remove(publication22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Publication22> findPublication22Entities() {
        return findPublication22Entities(true, -1, -1);
    }

    public List<Publication22> findPublication22Entities(int maxResults, int firstResult) {
        return findPublication22Entities(false, maxResults, firstResult);
    }

    private List<Publication22> findPublication22Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Publication22 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Publication22 findPublication22(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Publication22.class, id);
        } finally {
            em.close();
        }
    }

    public int getPublication22Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Publication22 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
