/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture5.ex2_with_controllers.controllers;

import ____w22.lecture5.ex1.entities2.Magazine22;
import ____w22.lecture5.ex2_with_controllers.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author fcarella
 */
public class Magazine22JpaController implements Serializable {

    public Magazine22JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Magazine22 magazine22) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(magazine22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Magazine22 magazine22) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            magazine22 = em.merge(magazine22);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = magazine22.getId();
                if (findMagazine22(id) == null) {
                    throw new NonexistentEntityException("The magazine22 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Magazine22 magazine22;
            try {
                magazine22 = em.getReference(Magazine22.class, id);
                magazine22.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The magazine22 with id " + id + " no longer exists.", enfe);
            }
            em.remove(magazine22);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Magazine22> findMagazine22Entities() {
        return findMagazine22Entities(true, -1, -1);
    }

    public List<Magazine22> findMagazine22Entities(int maxResults, int firstResult) {
        return findMagazine22Entities(false, maxResults, firstResult);
    }

    private List<Magazine22> findMagazine22Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Magazine22 as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Magazine22 findMagazine22(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Magazine22.class, id);
        } finally {
            em.close();
        }
    }

    public int getMagazine22Count() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Magazine22 as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
