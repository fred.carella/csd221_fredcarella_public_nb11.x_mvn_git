/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture3.w20.ex1;

/**
 *
 * @author student
 */
public class App {

    public void run() {
        Person p = new Person();
        System.out.println(p);
        
        Person p2=new Person("Fred", "Carella");
        System.out.println(p2);
        
        Employee e1=new Employee();
        System.out.println(e1);

        Employee e2=new Employee("Sault College");
        System.out.println(e2);

        Employee e3=new Employee("Steve", "Spielberg", "Hollywood");
        System.out.println(e3);

    }
}
