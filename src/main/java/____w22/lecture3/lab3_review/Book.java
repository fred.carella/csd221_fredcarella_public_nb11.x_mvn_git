/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture3.lab3_review;

/**
 *
 * @author fcarella
 */
public class Book extends Publication{
    private String author;

    public Book() {
    }

    public Book(String author) {
        this.author = author;
    }

    public Book(String author, String title, int copies, double price) {
        super(title, copies, price);
        this.author = author;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
