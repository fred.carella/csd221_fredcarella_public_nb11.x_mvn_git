/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture3.lab3_review;

/**
 *
 * @author fcarella
 */
public class Magazine extends Publication {
    private int orderQty;
    private String currIssue;

    public Magazine() {
    }

    public Magazine(int orderQty, String currIssue, String title, int copies, double price) {
        super(title, copies, price);
        this.orderQty = orderQty;
        this.currIssue = currIssue;
    }

    public Magazine(int orderQty, String currIssue) {
        this.orderQty = orderQty;
        this.currIssue = currIssue;
    }

    /**
     * @return the orderQty
     */
    public int getOrderQty() {
        return orderQty;
    }

    /**
     * @param orderQty the orderQty to set
     */
    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * @return the currIssue
     */
    public String getCurrIssue() {
        return currIssue;
    }

    /**
     * @param currIssue the currIssue to set
     */
    public void setCurrIssue(String currIssue) {
        this.currIssue = currIssue;
    }
    
    public void adjustQty(int n){
    
    }
    public void receiveNewIssue(String s){
    }
    
    
}
