package ____w22.lecture2.jeddict.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author fcarella
 */
@Entity
public class Employee extends Person implements Serializable {

    @Basic
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}