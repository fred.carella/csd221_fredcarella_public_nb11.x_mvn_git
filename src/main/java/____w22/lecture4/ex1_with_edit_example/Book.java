/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture4.ex1_with_edit_example;

/**
 *
 * @author fcarella
 */
public class Book extends Publication{
    private String author;
    @Override
    public String toString() {
        return getTitle()+" by,  "+getAuthor()+" Price: "+getPrice();
    }

    @Override
    public void sellCopy() {
        System.out.println("selling book");
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }
    
}
