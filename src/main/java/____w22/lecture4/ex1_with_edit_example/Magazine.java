/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture4.ex1_with_edit_example;

/**
 *
 * @author fcarella
 */
public class Magazine extends Publication{
    @Override
    public String toString() {
        return "Selling a Magazine";
    }

    @Override
    public void sellCopy() {
        System.out.println("selling a Magazine");
    }
    
}
