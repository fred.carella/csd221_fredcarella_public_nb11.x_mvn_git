/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture4.ex1_with_edit_example;

/**
 *
 * @author fcarella
 */
public class CashTill {
    private double runningTotal;
    public void sellItem(SaleableItem item){
        runningTotal+=item.getPrice();
        item.sellCopy();
        System.out.println(item+" for: "+item.getPrice());
    }
    
    public void showTotal(){
        System.out.println("Total="+runningTotal);
    }
}
