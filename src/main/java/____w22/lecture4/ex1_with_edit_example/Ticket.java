/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture4.ex1_with_edit_example;

/**
 *
 * @author fcarella
 */
public class Ticket implements SaleableItem {
    private double price;

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void sellCopy() {
        System.out.println("printing a ticket");
    }
    
    
    
}
