/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____w22.lecture4.ex1_with_edit_example;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {

    Scanner input = new Scanner(System.in);

    public App() {
    }

    public void run() {
        Publication p = new Book();

        Book book = new Book();
        book.setTitle("Intro 2 Java");
        book.setAuthor("Marcilino D");
        Magazine mag = new Magazine();
        DiscMag dMag = new DiscMag();
        CashTill till = new CashTill();
        Journal journ = new Journal();
        Ticket ticket = new Ticket();
        Pen pen = new Pen();

        book.setPrice(1.0);
        mag.setPrice(2.0);
        dMag.setPrice(3.0);
        journ.setPrice(4.0);
        ticket.setPrice(5.0);

        till.sellItem(book);
        till.sellItem(mag);
        till.sellItem(dMag);
        till.sellItem(journ);
        till.sellItem(ticket);
        till.sellItem(pen);

        System.out.println("Book = " + book);
        edit(book);
        System.out.println("Book = " + book);

        till.showTotal();

    }

    private void edit(Book book) {
        System.out.println("Please edit the book");
        System.out.println("Edit Author: " + book.getAuthor());
        book.setAuthor(getInput(book.getAuthor()));
        System.out.println("Edit Title: " + book.getTitle());
        book.setTitle(getInput(book.getTitle()));

        System.out.println("Price: " + book.getPrice());
        book.setPrice(getInput(book.getPrice()));

//        list();
//        System.out.println("Which car would you like to edit ?:");
//        int choice = input.nextInt();
//        input = new Scanner(System.in); // reset the scanner
//        if ((choice < currentIndex + 1) && choice > 0) {
//            Car c = cars[choice - 1];
//            System.out.println("Make: " + c.getMake());
//            c.setMake(getInput(c.getMake()));
//            System.out.println("Model: " + c.getModel());
//            c.setModel(getInput(c.getModel()));
//            System.out.println("Year: " + c.getYear());
//            c.setYear(getInput(c.getYear()));
//        } else {
//            System.out.println("Choice out of bounds");
//        }
//        System.out.println("");
    }

    private String getInput(String s) {
        String ss = input.nextLine();
        if (ss.trim().isEmpty()) {
            return s;
        }
        Scanner in2 = new Scanner(ss);
        return in2.nextLine();
    }

    private int getInput(int i) {
        String s = input.nextLine();
        if (s.trim().isEmpty()) {
            return i;
        }
        Scanner in2 = new Scanner(s);
        return in2.nextInt();
    }

    private double getInput(double i) {
        String s = input.nextLine();
        if (s.trim().isEmpty()) {
            return i;
        }
        Scanner in2 = new Scanner(s);
        return in2.nextDouble();
    }
    private boolean getInput(boolean i) {
        String s = input.nextLine();
        if (s.trim().isEmpty()) {
            return i;
        }
        Scanner in2 = new Scanner(s);
        return in2.nextBoolean();
    }

}
