/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20.lecture5.controllers;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import w20.lecture5.controllers.exceptions.NonexistentEntityException;
import w20.lecture5.entities.DiscMagazine;

/**
 *
 * @author student
 */
public class DiscMagazineJpaController implements Serializable {

    public DiscMagazineJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DiscMagazine discMagazine) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(discMagazine);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DiscMagazine discMagazine) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            discMagazine = em.merge(discMagazine);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = discMagazine.getId();
                if (findDiscMagazine(id) == null) {
                    throw new NonexistentEntityException("The discMagazine with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DiscMagazine discMagazine;
            try {
                discMagazine = em.getReference(DiscMagazine.class, id);
                discMagazine.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The discMagazine with id " + id + " no longer exists.", enfe);
            }
            em.remove(discMagazine);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DiscMagazine> findDiscMagazineEntities() {
        return findDiscMagazineEntities(true, -1, -1);
    }

    public List<DiscMagazine> findDiscMagazineEntities(int maxResults, int firstResult) {
        return findDiscMagazineEntities(false, maxResults, firstResult);
    }

    private List<DiscMagazine> findDiscMagazineEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from DiscMagazine as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DiscMagazine findDiscMagazine(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DiscMagazine.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiscMagazineCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from DiscMagazine as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
