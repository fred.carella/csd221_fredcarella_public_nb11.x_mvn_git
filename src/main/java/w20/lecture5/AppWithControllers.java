/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20.lecture5;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import w20.lecture5.controllers.BookJpaController;
import w20.lecture5.entities.Book;
import w20.lecture5.entities.Publication;


/**
 *
 * @author student
 */
public class AppWithControllers {

    void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("W20_LECTURE5_PU");
            em = emf.createEntityManager();
            em.getTransaction().begin();

            ////////////// CREATE BOOK WITHOUT CONTROLLER
            Book book1 = new Book();
            book1.setAuthor("Me");
            book1.setCopies(5);
            book1.setTitle("Java is fun!");
            book1.setPrice(9.50);
            em.persist(book1);
            em.getTransaction().commit();
            ////////////// 
            
            List<Publication> listOfPublications = em.createQuery("SELECT c FROM Publication c").getResultList();
            System.out.println("List of Publications");
            for(Publication customer:listOfPublications){
                System.out.println(customer.getTitle());
            }
            List<Book> listOfBooks = em.createQuery("SELECT c FROM Book c").getResultList();
            System.out.println("List of Books");
            for(Book customer:listOfBooks){
                System.out.println(customer.getTitle());
            }


            ////////////// CREATE BOOK WITH CONTROLLER
            BookJpaController book1JpaController = new BookJpaController(emf);

            Book book2 = new Book();
            book2.setAuthor("Me");
            book2.setCopies(5);
            book2.setTitle("Java is Super Fun! 2e");
            book2.setPrice(19.50);
            book1JpaController.create(book2);

            ////////////// LIST BOOKS WITH CONTROLLER
            List<Book> books = book1JpaController.findBookEntities();
            for (Book b : books) {
                System.out.println(b.getTitle());
            }
            ////////////// EDIT BOOK WITH CONTROLLER
            for (Book b : books) {
                b.setTitle(book1.getTitle() + "_" + System.currentTimeMillis());
                book1JpaController.edit(b);
            }
            books = book1JpaController.findBookEntities();
            for (Book b : books) {
                System.out.println(b.getTitle());
            }
            ////////////// DELETE BOOKS WITH CONTROLLER
            for (Book b : books) {
//                book1JpaController.destroy(b.getId());
            }

            ////////////////////
        } catch (Exception e) {
            Logger.getLogger(lecture4b_jpa.Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }
    
}
