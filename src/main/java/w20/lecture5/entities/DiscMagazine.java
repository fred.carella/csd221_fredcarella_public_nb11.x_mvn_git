package w20.lecture5.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author student
 */

@Entity
public class DiscMagazine extends Magazine {

    @Basic
    private boolean hasDisk;

    public DiscMagazine() {
    }

    public DiscMagazine(int orderQty, String currIssue, boolean hasDisk) {
        super(orderQty, currIssue);
        this.hasDisk = hasDisk;
    }

    public boolean isHasDisk() {
        return hasDisk;
    }

    public void setHasDisk(boolean hasDisk) {
        this.hasDisk = hasDisk;
    }

    @Override
    public void recvNewIssue(String currIssue) {
        // To change body of generated methods, choose Tools | Templates.
        super.recvNewIssue(currIssue);
    }

}