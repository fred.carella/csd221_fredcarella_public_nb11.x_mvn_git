/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___w21__brett;

import ___w21__brett.entities.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author students
 */
public class App {
    public void run(){
        //brett_DEFAULT_PU
        EntityManagerFactory emf=null;
        EntityManager em=null;
        
        try {
            emf = Persistence.createEntityManagerFactory("brett_DEFAULT_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created (" + emf + ")");
            em.getTransaction().begin();
            
            Book_Brett book=new Book_Brett();
            book.setAuthor("Brett");
            book.setTitle("Bretts Book");
            book.setPrice(12.34);
            
            Magazine_Brett mag=new Magazine_Brett();
            mag.setTitle("Mag Title");
            mag.setPrice(12.89);
            
            em.persist(book);
            em.persist(mag);
            
             em.getTransaction().commit();
        }catch(Exception e){
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            if(emf!=null)
                emf.close();
//            if(em!=null)
//                em.close();
        }
    }
}
