/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___w21__brett;

import ___w21__brett.controllers.Book_BrettJpaController;
import ___w21__brett.controllers.Magazine_BrettJpaController;
import ___w21__brett.entities.Book_Brett;
import ___w21__brett.entities.Magazine_Brett;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author students
 */
public class AppWithControllers {

    public void run() {
        EntityManagerFactory emf = null;

        try {
            emf = Persistence.createEntityManagerFactory("brett_DEFAULT_PU");
            
            Book_Brett book=new Book_Brett();
            book.setAuthor("Brett");
            book.setTitle("Bretts Book");
            book.setPrice(12.34);
            
            Magazine_Brett mag=new Magazine_Brett();
            mag.setTitle("Mag Title");
            mag.setPrice(12.89);
            
            Book_BrettJpaController bc=new Book_BrettJpaController(emf);
            Magazine_BrettJpaController mc=new Magazine_BrettJpaController(emf);
            
            bc.create(book);
            mc.create(mag);
            
            book.setTitle("New Title");
            bc.edit(book);
            
            List<Book_Brett> books=bc.findBook_BrettEntities();
            
            int i=1;
            for(Book_Brett b:books){
                System.out.println(i+"."+b.getTitle());
                i++;
            }
            
            
        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }
}
