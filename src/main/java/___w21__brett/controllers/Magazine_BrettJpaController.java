/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___w21__brett.controllers;

import ___w21__brett.controllers.exceptions.NonexistentEntityException;
import ___w21__brett.entities.Magazine_Brett;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Magazine_BrettJpaController implements Serializable {

    public Magazine_BrettJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Magazine_Brett magazine_Brett) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(magazine_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Magazine_Brett magazine_Brett) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            magazine_Brett = em.merge(magazine_Brett);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = magazine_Brett.getId();
                if (findMagazine_Brett(id) == null) {
                    throw new NonexistentEntityException("The magazine_Brett with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Magazine_Brett magazine_Brett;
            try {
                magazine_Brett = em.getReference(Magazine_Brett.class, id);
                magazine_Brett.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The magazine_Brett with id " + id + " no longer exists.", enfe);
            }
            em.remove(magazine_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Magazine_Brett> findMagazine_BrettEntities() {
        return findMagazine_BrettEntities(true, -1, -1);
    }

    public List<Magazine_Brett> findMagazine_BrettEntities(int maxResults, int firstResult) {
        return findMagazine_BrettEntities(false, maxResults, firstResult);
    }

    private List<Magazine_Brett> findMagazine_BrettEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Magazine_Brett as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Magazine_Brett findMagazine_Brett(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Magazine_Brett.class, id);
        } finally {
            em.close();
        }
    }

    public int getMagazine_BrettCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Magazine_Brett as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
