/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___w21__brett.controllers;

import ___w21__brett.controllers.exceptions.NonexistentEntityException;
import ___w21__brett.entities.DiscMag_Brett;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class DiscMag_BrettJpaController implements Serializable {

    public DiscMag_BrettJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DiscMag_Brett discMag_Brett) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(discMag_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DiscMag_Brett discMag_Brett) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            discMag_Brett = em.merge(discMag_Brett);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = discMag_Brett.getId();
                if (findDiscMag_Brett(id) == null) {
                    throw new NonexistentEntityException("The discMag_Brett with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DiscMag_Brett discMag_Brett;
            try {
                discMag_Brett = em.getReference(DiscMag_Brett.class, id);
                discMag_Brett.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The discMag_Brett with id " + id + " no longer exists.", enfe);
            }
            em.remove(discMag_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DiscMag_Brett> findDiscMag_BrettEntities() {
        return findDiscMag_BrettEntities(true, -1, -1);
    }

    public List<DiscMag_Brett> findDiscMag_BrettEntities(int maxResults, int firstResult) {
        return findDiscMag_BrettEntities(false, maxResults, firstResult);
    }

    private List<DiscMag_Brett> findDiscMag_BrettEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from DiscMag_Brett as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DiscMag_Brett findDiscMag_Brett(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DiscMag_Brett.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiscMag_BrettCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from DiscMag_Brett as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
