/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___w21__brett.controllers;

import ___w21__brett.controllers.exceptions.NonexistentEntityException;
import ___w21__brett.entities.Book_Brett;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Book_BrettJpaController implements Serializable {

    public Book_BrettJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Book_Brett book_Brett) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(book_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Book_Brett book_Brett) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            book_Brett = em.merge(book_Brett);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = book_Brett.getId();
                if (findBook_Brett(id) == null) {
                    throw new NonexistentEntityException("The book_Brett with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Book_Brett book_Brett;
            try {
                book_Brett = em.getReference(Book_Brett.class, id);
                book_Brett.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The book_Brett with id " + id + " no longer exists.", enfe);
            }
            em.remove(book_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Book_Brett> findBook_BrettEntities() {
        return findBook_BrettEntities(true, -1, -1);
    }

    public List<Book_Brett> findBook_BrettEntities(int maxResults, int firstResult) {
        return findBook_BrettEntities(false, maxResults, firstResult);
    }

    private List<Book_Brett> findBook_BrettEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Book_Brett as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Book_Brett findBook_Brett(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Book_Brett.class, id);
        } finally {
            em.close();
        }
    }

    public int getBook_BrettCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Book_Brett as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
