/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___w21__brett.controllers;

import ___w21__brett.controllers.exceptions.NonexistentEntityException;
import ___w21__brett.entities.Ticket_Brett;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Ticket_BrettJpaController implements Serializable {

    public Ticket_BrettJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ticket_Brett ticket_Brett) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ticket_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ticket_Brett ticket_Brett) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ticket_Brett = em.merge(ticket_Brett);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = ticket_Brett.getId();
                if (findTicket_Brett(id) == null) {
                    throw new NonexistentEntityException("The ticket_Brett with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ticket_Brett ticket_Brett;
            try {
                ticket_Brett = em.getReference(Ticket_Brett.class, id);
                ticket_Brett.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ticket_Brett with id " + id + " no longer exists.", enfe);
            }
            em.remove(ticket_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ticket_Brett> findTicket_BrettEntities() {
        return findTicket_BrettEntities(true, -1, -1);
    }

    public List<Ticket_Brett> findTicket_BrettEntities(int maxResults, int firstResult) {
        return findTicket_BrettEntities(false, maxResults, firstResult);
    }

    private List<Ticket_Brett> findTicket_BrettEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Ticket_Brett as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ticket_Brett findTicket_Brett(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ticket_Brett.class, id);
        } finally {
            em.close();
        }
    }

    public int getTicket_BrettCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Ticket_Brett as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
