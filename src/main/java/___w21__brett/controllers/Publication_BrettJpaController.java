/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___w21__brett.controllers;

import ___w21__brett.controllers.exceptions.NonexistentEntityException;
import ___w21__brett.entities.Publication_Brett;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author students
 */
public class Publication_BrettJpaController implements Serializable {

    public Publication_BrettJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Publication_Brett publication_Brett) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(publication_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Publication_Brett publication_Brett) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            publication_Brett = em.merge(publication_Brett);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = publication_Brett.getId();
                if (findPublication_Brett(id) == null) {
                    throw new NonexistentEntityException("The publication_Brett with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Publication_Brett publication_Brett;
            try {
                publication_Brett = em.getReference(Publication_Brett.class, id);
                publication_Brett.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The publication_Brett with id " + id + " no longer exists.", enfe);
            }
            em.remove(publication_Brett);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Publication_Brett> findPublication_BrettEntities() {
        return findPublication_BrettEntities(true, -1, -1);
    }

    public List<Publication_Brett> findPublication_BrettEntities(int maxResults, int firstResult) {
        return findPublication_BrettEntities(false, maxResults, firstResult);
    }

    private List<Publication_Brett> findPublication_BrettEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Publication_Brett as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Publication_Brett findPublication_Brett(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Publication_Brett.class, id);
        } finally {
            em.close();
        }
    }

    public int getPublication_BrettCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Publication_Brett as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
