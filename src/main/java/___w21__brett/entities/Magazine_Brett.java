package ___w21__brett.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author students
 */
@Entity
public class Magazine_Brett extends Publication_Brett implements Serializable {

    @Basic
    private int orderQty;

    public int getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

}