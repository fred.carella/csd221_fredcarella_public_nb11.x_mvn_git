package ___w21__brett.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author students
 */
@Entity
public class DiscMag_Brett extends Magazine_Brett implements Serializable {

    @Basic
    private boolean hasDisc;

    public boolean isHasDisc() {
        return hasDisc;
    }

    public void setHasDisc(boolean hasDisc) {
        this.hasDisc = hasDisc;
    }

}