package _______EricTest_2.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author fcarella
 */
@Entity
public class Magazine extends Publication {

    @Basic
    private int orderQty;
    @Basic
    private String curIssue;

    public int getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    public String getCurIssue() {
        return curIssue;
    }

    public void setCurIssue(String curIssue) {
        this.curIssue = curIssue;
    }

}