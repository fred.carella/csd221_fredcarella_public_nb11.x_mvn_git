package _______EricTest_2.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author fcarella
 */
@Entity
public class Pencil {

    @Id
    @GeneratedValue
    private Long id;
    @Basic
    private String pencilAtt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPencilAtt() {
        return pencilAtt;
    }

    public void setPencilAtt(String pencilAtt) {
        this.pencilAtt = pencilAtt;
    }

}